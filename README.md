# Personal Website

This is a blog and personal website to both serve as a portfolio as well as a place to monitor my learning process.
The website is hosted via Gitlab Pages and is built upon the Hugo open-source static site generator.
Please feel free to peruse my [site](https://spencerraisanen.gitlab.io/) at your leisure.
