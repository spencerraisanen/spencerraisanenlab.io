Based in Phoenix, AZ.

### Primary Focus Areas

- Blockchain Development (new focus & I am determined to succeed)
- Business Development (Utilize data to make relevant decisions for business purposes)
- Data (Engineering, Analysis, Mining, Visualization, etc...)
- Statistical Analysis & Learning (AKA Machine Learning)

### Programming

- Python
- R
- Javascript
- C++
- SQL
- Unix/Linux

### Interests

- Blockchain
- Economics
- Backpacking
- Building
