---
title: About Me
subtitle: Curious, Unconventional, and Motivated
comments: false
---

My name is Spencer Raisanen.

I am a dual national of the USA and Finland who was born in MN, USA but has lived for extensive periods in both countries.

I am an experienced Analyst with additional experience in Engineering and Mathematical/Physical Modeling. I am an independent, big-picture thinker who loves a challenge.
