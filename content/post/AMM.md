---
subtitle: Simple Math & Incredible Results
date: 2021-05-25
tags: [AMM]
katex: true
markup: mmark
---

$$x \times y = k$$.

Look at that again.

$$x \times y = k$$.

![Uniswap Price Curve (k=1)](/img/UniswapCurve.png)

This is the main formula behind Uniswap pricing called the Constant Product Formula. It is a formula which has allowed a fully decentralized platform to successfully trade (sometimes with high slippage due to the trade size and subsequent move along the price curve) billions of USD in daily volume in place of a traditional order book.

Sometimes, the simple answer may just be a great one. The formula itself is quite simple: $$k$$ is a constant which is set to the swap rate that the pool requires and $$x$$ and $$y$$ are simply the amounts of each token required to meet that liquidity condition. When we look at this formula it becomes quite obvious that large orders or large quantities of orders removing a large portion of one of the tokens can affect the amount of the other token required to meet $$k$$. This affects the ratio and thus the price when swapping between the two assets.

Although the introduction of Uniswap V3 has introduced new features (and complexities) surrounding this simple formula, the Constant Product Formula has remained... constant.

<!--more-->
